<?php
   // we'll begin by assigning the To address and message subject
   $to="info@plantengrg.com";

   $subject="Employee details";

   // get the sender's name and email address
   // we'll just plug them a variable to be used later
   $from = stripslashes($_POST['first'])."<".stripslashes($_POST['emailAddress']).">";

   if(empty($_POST['first'])  || empty($_POST['emailAddress']) )
    {
		$errors .= "\n Error: all fields are required";
		header("Location: careers.php?error");
	}

	@extract($_POST);
	$name = $_POST['first'];
	$email_address = $_POST['emailAddress'];
	$middle=$_POST['middle'];
	$last=$_POST['last'];
	$day=$_POST['day'];
	$month=$_POST['month'];
	$year=$_POST['year'];
	$area=$_POST['area'];
	$phone=$_POST['phone'];
	$a="";
	$q209howWere = $_POST['q209howWere'];
	foreach ( $q209howWere as $howWere )
	{
		$a=$howWere." ".$a;
	}
	$addrline1=$_POST['addrline1'];
	$addrline2=$_POST['addrline2'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$postal=$_POST['postal'];
	$country=$_POST['country'];
	$describeYour196=$_POST['describeYour196'];
	$trainingOr=$_POST['trainingOr'];
	$first1=$_POST['first1'];
	$last1=$_POST['last1'];
	$area1=$_POST['area1'];
	$phoneNumber1=$_POST['phoneNumber1'];
	$yearsKnown1=$_POST['yearsKnown1'];
	$addrline11=$_POST['addrline11'];
	$addrline21=$_POST['addrline21'];
	$city1=$_POST['city1'];
	$state1=$_POST['state1'];
	$postal1=$_POST['postal1'];
	$country1=$_POST['country1'];
	$first2=$_POST['first2'];
	$last2=$_POST['last2'];
	$area2=$_POST['area2'];
	$phoneNumber2=$_POST['phoneNumber2'];
	$yearsKnown2=$_POST['yearsKnown2'];
	$addrline21=$_POST['addrline21'];
	$addrline22=$_POST['addrline22'];
	$city2=$_POST['city2'];
	$state2=$_POST['state2'];
	$postal2=$_POST['postal2'];
	$country2=$_POST['country2'];
	// generate a random string to be used as the boundary marker
	$mime_boundary="==Multipart_Boundary_x".md5(mt_rand())."x";

	// store the file information to variables for easier access
	$tmp_name = $_FILES['uploadResume']['tmp_name'];
	$type = $_FILES['uploadResume']['type'];
	$file_name = $_FILES['uploadResume']['name'];
	$size = $_FILES['uploadResume']['size'];
	// here we'll hard code a text message
	// again, in reality, you'll normally get this from the form submission
	$message = '
	First Name: '.$first.'
	Middle Name: '.$middle.'
	Last Name: '.$last.'
	Birth Date: '.$day.' '.$month.' '.$year.'
	Phone Number: '.$area.'-'. $phone.'
	E-mail Address: '.$emailAddress.'
	Address: '.$addrline1.','.$addrline2.','.$city.','.$state.','.$postal.','.$country.'
	How were you referred to us: '.$a.'
	Describe your skills: '.$describeYour196.'
	Training or Certifications: '.$trainingOr.'
	Reference 1:-
		Name: '.$first1.' '.$last1.'
		Phone Number: '.$area1.'-'. $phoneNumber1.'
		Years Known: '.$yearsKnown1.'
		Address: '.$addrline11.','.$addrline12.','.$city1.','.$state1.','.$postal1.','.$country1.'
	Reference 2:-
		Name: '.$first2.' '.$last2.'
		Phone Number: '.$area2.'-'. $phoneNumber2.'
		Years Known: '.$yearsKnown2.'
		Address: '.$addrline21.','.$addrline22.','.$city2.','.$state2.','.$postal2.','.$country2;


	// if the upload succeded, the file will exist
	if (file_exists($tmp_name)){

		// check to make sure that it is an uploaded file and not a system file
		if(is_uploaded_file($tmp_name)){

			// open the file for a binary read
			$file = fopen($tmp_name,'rb');

			// read the file content into a variable
			$data = fread($file,filesize($tmp_name));

			// close the file
			fclose($file);

			// now we encode it and split it into acceptable length lines
			$data = chunk_split(base64_encode($data));
		}

		// now we'll build the message headers
		$headers = "From: $from\r\n" .
		 "MIME-Version: 1.0\r\n" .
		 "Content-Type: multipart/mixed;\r\n" .
		 " boundary=\"{$mime_boundary}\"";

		// next, we'll build the message body
		// note that we insert two dashes in front of the
		// MIME boundary when we use it
		$message = "This is a multi-part message in MIME format.\n\n" .
		 "--{$mime_boundary}\n" .
		 "Content-Type: text/plain; charset=\"iso-8859-1\"\n" .
		 "Content-Transfer-Encoding: 7bit\n\n" .
		 $message . "\n\n";

		// now we'll insert a boundary to indicate we're starting the attachment
		// we have to specify the content type, file name, and disposition as
		// an attachment, then add the file content and set another boundary to
		// indicate that the end of the file has been reached
		$message .= "--{$mime_boundary}\n" .
		 "Content-Type: {$type};\n" .
		 " name=\"{$file_name}\"\n" .
		 //"Content-Disposition: attachment;\n" .
		 //" filename=\"{$fileatt_name}\"\n" .
		 "Content-Transfer-Encoding: base64\n\n" .
		 $data . "\n\n" .
		 "--{$mime_boundary}--\n";

		// now we just send the message
		if (mail($to, $subject, $message, $headers)){
		 $yes="Mail sent";
		header("Location: careers.php?mes=$yes");}
		  else{
			 $not="Mail not sent";
		header("Location: careers.php?mes=$not");}
	}
	else {
		header("Location: careers.php?resume");
	}
?>